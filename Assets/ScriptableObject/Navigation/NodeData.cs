﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class NodeData : ScriptableObject
{
    public Vector2Int   PositionGrid;

    [Header("Sprite Management")]
    [SerializeField]
    private Sprite       m_defaultSprite = null;
    [SerializeField]
    private Sprite       m_playerSprite = null;
    [SerializeField]
    private List<Sprite> m_spritesList   = new List<Sprite>();
    private Sprite       m_sprite        = null;
    /*
    [Header("Encounter Management")]*/

    /// <summary>
    /// Get current Node Data Sprite.
    /// </summary>
    public Sprite        Sprite 
    {
        get 
        {
            if(m_sprite == null)
                m_sprite = SelectSprite();

            return m_sprite; 
        }
    }
    /// <summary>
    /// Get current default sprite
    /// </summary>
    public Sprite        DefaultSprite { get => m_defaultSprite; }
    /// <summary>
    /// Get current Player sprite
    /// </summary>
    public Sprite        PlayerSprite { get => m_playerSprite; }

    private Sprite SelectSprite()
    {
        if(m_spritesList == null || m_spritesList.Count == 0)
            return m_defaultSprite;

        int index = (m_spritesList.Count == 1) ? 0 : UnityEngine.Random.Range(0, m_spritesList.Count);

        return m_spritesList[index];
    }
}