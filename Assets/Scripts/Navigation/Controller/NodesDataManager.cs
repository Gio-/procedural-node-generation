﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodesDataManager : MonoBehaviour
{
    public static NodesDataManager Instance = null;                         /// Instance of this script.
    public bool   DoNotDestroyOnLoad        = false;
    [Space(20)]

    [SerializeField]
    private NodesDataModel  m_nodesDataModel  = new NodesDataModel();       /// Model
    [SerializeField]    
    private List<NodeData>  m_nodesData;                                    /// Container of node data.
     
    public delegate void NodesDataGeneratedHandler(List<NodeData> data);
    public event NodesDataGeneratedHandler OnNodesGeneratedEvent;

    #region UnityMethods
    void Awake()
    {
        #region Singleton
        if(Instance == null)
        {
            Instance = this;
        }
        else if(Instance != this)
        {
            Destroy(Instance);
            return;
        }

        if(DoNotDestroyOnLoad)
        DontDestroyOnLoad(this.gameObject);
        #endregion
    }
    void OnEnable()
    {
        MapController.OnMapGenerationCompleteEvent += OnMapGenerated;
    }

    void OnDisable()
    {
        MapController.OnMapGenerationCompleteEvent -= OnMapGenerated;
    }
    #endregion

    /// <summary>
    /// Event callback after map is generated.
    /// </summary>
    /// <param name="StartNode">Starting node</param>
    /// <param name="ExitNode">End node</param>
    /// <param name="MaxNodes">Total node generated.</param>
    private void OnMapGenerated(Vector2Int StartNode, Vector2Int ExitNode, int MaxNodes)
    {
        NodeData[] data;
        
        if(!m_nodesDataModel.GetNodesToSpawn(MaxNodes, out data))
            return;
        
        m_nodesData = new List<NodeData>();
        m_nodesData.AddRange(data);

        int index = 0;
        
        for (int y = 0; y <= MapController.Instance.Map.GridSize.y; y++)
        {
            for (int x = 0; x <= MapController.Instance.Map.GridSize.x; x++)
            {
                if (MapController.Instance.Map.Grid.Contains(new Vector2Int(x, y)))
                {
                    m_nodesData[index].PositionGrid = new Vector2Int(x,y);
                    index ++;
                }
            }
        }

        if(OnNodesGeneratedEvent != null)
            OnNodesGeneratedEvent(m_nodesData);
    }
    
    /// <summary>
    /// Get a node Data from position.
    /// </summary>
    /// <param name="position">Position to check.</param>
    /// <param name="data">Node data founded.</param>
    /// <returns>return true if it found something, false if not.</returns>
    public bool GetNodeDataFromPosition(Vector2Int position, out NodeData data)
    {
        data = null;
        if(m_nodesData == null || m_nodesData.Count == 0)
            return false;
        
        int index = m_nodesData.FindIndex((x)=>x.PositionGrid == position);
        
        if(index < 0)
            return false;

        data = m_nodesData[index];

        return true;
    }

}
