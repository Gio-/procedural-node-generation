﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationController : MonoBehaviour
{
    /// <summary>
    /// Travel Request result code.
    /// </summary>
    public enum TravelResultCode
    {
        VALID,
        NO_RESOURCE,
        INVALID_TRAVEL,
        ALREADY_IN_TRAVEL
    }

    [SerializeField]
    private Vector2Int  m_currentNodePosition;                              /// Current Node Position
    [SerializeField]
    private Vector2Int  m_currentDestination;                               /// Current Destination
    
    [SerializeField]
    private bool        m_inTravel;                                         /// Check if is in travel

    private Validator<Vector2Int, TravelResultCode>   m_validator;          /// Add rules to validate travel.

    /// <summary>
    /// Get current Node Position
    /// </summary>
    /// <value>return the node position in grid.</value>
    public Vector2Int   CurrentNodePos  { get => m_currentNodePosition; }
    /// <summary>
    /// Check if object is in travel or not.
    /// </summary>
    /// <value>return true if is travelling or false if not.</value>
    public bool         IsInTravel      { get => m_inTravel;            }

    public delegate void OnTravelStartHandler(Vector2Int StartNode, Vector2Int EndNode);
    public static event OnTravelStartHandler OnTravelStartEvent;

    public delegate void OnTravelDeniedHandler(TravelResultCode error);
    public static event OnTravelDeniedHandler OnTravelDeniedEvent;
    
    public delegate void OnPositionChangedHandler(Vector2Int position);
    public static event OnPositionChangedHandler OnPositionChangedEvent;

    #region UnityMethods
    void Start()
    {
        /// Set a bunch of rules that player has to respect if 
        /// the travel is requested.    
        m_validator = new Validator<Vector2Int, TravelResultCode>();
        m_validator.AddRule((x)=> { return !(m_inTravel); }, TravelResultCode.ALREADY_IN_TRAVEL);
        m_validator.AddRule((x)=> MapController.Instance.IsValidTravel(m_currentNodePosition, x), TravelResultCode.INVALID_TRAVEL);
    }

    void OnEnable()
    {
        MapController.OnMapGenerationCompleteEvent += OnMapGenerated;
    }

    void OnDisable()
    {
        MapController.OnMapGenerationCompleteEvent -= OnMapGenerated;
    }
    #endregion

    /// <summary>
    /// Request Travel to make to a node.
    /// </summary>
    /// <param name="to">Grid position of the node</param>
    /// <returns>return an enum with result.</returns>
    public TravelResultCode RequestTravel(Vector2Int to)
    {
        TravelResultCode result = TravelResultCode.VALID;

        if(!m_validator.IsValid(to, out result))
        {
            if(OnTravelDeniedEvent != null)
                OnTravelDeniedEvent(result);

            return result;
        }

        if(OnTravelStartEvent != null)
            OnTravelStartEvent(m_currentNodePosition, to);

        m_inTravel               = true;
        m_currentDestination     = to;

        return result;
    }

    /// <summary>
    /// Check if object can travel from current position to
    /// End positon
    /// </summary>
    /// <param name="to">End Position</param>
    /// <returns>return an enum with result.</returns>
    public TravelResultCode CanTravel(Vector2Int to)
    {
        TravelResultCode result = TravelResultCode.VALID;
        m_validator.IsValid(to, out result);
    
        return result;
    }

    /// <summary>
    /// End current Travel
    /// </summary>
    public void EndTravel()
    {
        if(!m_inTravel)
            return;

        m_currentNodePosition   = m_currentDestination;
        m_inTravel              = false;

        if(OnPositionChangedEvent != null)
            OnPositionChangedEvent(m_currentNodePosition);
    }
    
    /// <summary>
    /// Set current position to a new one.
    /// </summary>
    /// <param name="position">The new position to set.</param>
    public void SetCurrentPosition(Vector2Int position)
    {
        if(!MapController.Instance.IsValidNode(position))
            return;

        m_currentNodePosition = position;
        m_currentDestination  = position;
        m_inTravel            = false;

        if(OnPositionChangedEvent != null)
            OnPositionChangedEvent(position);
    }

    private void OnMapGenerated(Vector2Int startNode, Vector2Int exitNode, int totalNodes)
    {
        SetCurrentPosition(startNode);
    }

}
