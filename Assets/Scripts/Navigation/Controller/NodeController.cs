﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeController : MonoBehaviour
{
    public static NodePanelView NodePanel = null;                   /// Node Panel in scene
    public static NavigationController NavigationController = null; /// Player Navigation Controller

    [SerializeField]
    private NodeData  m_data;                                       /// The data linked with node
    public  NodeView  NodeView;                                     /// The node visualization in UI

    /// <summary>
    /// Node Data linked with the node.
    /// </summary>
    /// <value></value>
    public NodeData   Data
    {
        get { return m_data; }
        set 
        {
            if(m_data != value)
            {
                m_data = value;
                DataChange();
            }
        }
    }                                         
     
    /// <summary>
    /// Position of the node in Grid.
    /// </summary>
    /// <value></value>
    public Vector2Int GridPosition
    { 
        get { return (Data == null) ? new Vector2Int(-1, -1) : Data.PositionGrid; } 
    }

    public delegate void DataChangeHandler(NodeController controller, NodeData data);
    public event DataChangeHandler OnDataChangeEvent;

    #region Unity Methods
    void Awake()
    {
        if(NodePanel == null)
            NodePanel = FindObjectOfType<NodePanelView>();

        if(NodeView == null)
            NodeView = GetComponentInChildren<NodeView>();
        
        if(NodeView != null)
            if(NodeView.Button != null)
                NodeView.Button.onClick.AddListener(ActivatePanel);
        
        if(NavigationController == null)
            NavigationController = FindObjectOfType<NavigationController>();
    }
    
    void OnEnable()
    {
        NavigationController.OnPositionChangedEvent += OnPositionChanged;
    }

    void OnDisable()
    {
        NavigationController.OnPositionChangedEvent -= OnPositionChanged;
    }

    void OnDestroy()
    {
        if(NodeView != null)
            if(NodeView.Button != null)
                NodeView.Button.onClick.RemoveAllListeners();
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Callback Event on NodeView.Button click, activate
    /// panel on node click.
    /// </summary>
    public void ActivatePanel()
    {
        if(NodePanel == null) return;
        if(NavigationController == null)
            Debug.LogWarning("[NodeController](ActivatePanel): No Navigation Controller found");
        
        bool isTravelValid = NavigationController.CanTravel(GridPosition) == NavigationController.TravelResultCode.VALID;     

        NodePanel.ChangeDestination(GridPosition);
        NodePanel.ShowHideButton(isTravelValid);
        NodePanel.Show();
    }

    /// <summary>
    /// Check if this node is close to a position.
    /// </summary>
    /// <param name="position">Position to check.</param>
    /// <returns>True if close, false if not.</returns>
    public bool IsNodeCloseToPosition(Vector2Int position, float closeRange)
    {
        if(Data == null) return false;

        return Vector2Int.Distance(Data.PositionGrid, position) <= closeRange;
               /*&&
               MapController.Instance.IsValidTravel(Data.PositionGrid, position);*/
    }
    #endregion

    #region Private Methods
    /// <summary>
    /// Callback event called from NavigationController OnPositionChange
    /// </summary>
    /// <param name="position"></param>
    private void OnPositionChanged(Vector2Int position)
    {
        /// Change sprite based on new Player position.
        if(Data != null && NodeView != null)
        {
            if(NavigationController.CurrentNodePos == GridPosition)
            {
                NodeView.Sprite = Data.PlayerSprite;
            }
            else
            {
                NodeView.Sprite = IsNodeCloseToPosition(position, 1.0f) ? Data.Sprite : Data.DefaultSprite;    
            }
        }
    }

    protected virtual void DataChange()
    {
        /// Change Sprite
        OnPositionChanged(GridPosition);

        if(OnDataChangeEvent != null)
            OnDataChangeEvent(this, m_data);
    }
    #endregion
}
