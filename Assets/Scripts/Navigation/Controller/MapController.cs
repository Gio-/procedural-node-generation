﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProceduralGeneration;
using UnityEngine;

public class MapController : MonoBehaviour
{
    #region Singleton
    public static MapController Instance = null;
    public bool   DoNotDestroyOnLoad     = false;
    [Space(20)]
    #endregion

    private MapModel        m_mapModel;             /// Data Model to send input 
    private Map             m_map;                  /// Output of the Model

    [Header("Grid Setup")]
    [SerializeField]
    private Vector2Int      m_grid          = new Vector2Int(0, 0); /// Grid in which map is represented.
    [SerializeField]
    private int             m_maxNodes      = 0;    /// Max nodes in the grid.

    [Header("Start End Position")]
    [SerializeField, Range(0.0f, 1.0f)]
    private float           m_startPosition = 0.0f; /// Representation of the start position in float
    [SerializeField, Range(0.0f, 1.0f)]             
    private float           m_endPosition   = 1.0f; /// Representation of the end   position in float

    private Vector2Int      m_startNode;            /// Start Node
    private Vector2Int      m_endNode;              /// Exit  Node

    /// <summary>
    /// Get the Start Node position
    /// </summary>
    /// <value>Return the start node position in grid.</value>
    public Vector2Int       GetStartNode      { get => m_startNode;   }
    /// <summary>
    /// Get the Exit Node position 
    /// </summary>
    /// <value>Return the exit node position in grid.</value>
    public Vector2Int       GetEndNode         { get => m_endNode;     }

    public Map              Map                { get => m_map;         }

    public delegate void MapGenerationCompleteHandler(Vector2Int StartNode, Vector2Int ExitNode, int TotalNodes);
    public static event MapGenerationCompleteHandler OnMapGenerationCompleteEvent;

    private void Awake()
    {
        #region Singleton
        if(Instance == null) 
        {
            Instance = this;
        }else{
            Destroy(this.gameObject);
        }

        if(DoNotDestroyOnLoad)
            DontDestroyOnLoad(this.gameObject);

        #endregion
    }

    private void Start()
    {
        GenerateMap();
    }

    public void GenerateMap()
    {
        /// Generate a Map.
        m_mapModel  = new MapModel();
        m_map       = m_mapModel.GenerateMap(m_grid, m_maxNodes);

        /// Place Start and End Node.
        int nodeIndex = 0;
        int nodeStart = GetFixedNodeAtPosition(m_startPosition);
        int nodeEnd   = GetFixedNodeAtPosition(m_endPosition  );

        for (int y = 0; y <= m_map.GridSize.y; y++)
        {
            for (int x = 0; x <= m_map.GridSize.x; x++)
            {
                if (m_map.Grid.Contains(new Vector2Int(x, y)))
                {
                    /// Find First Node
                    if(nodeIndex == nodeStart)
                        m_startNode = new Vector2Int(x, y);
                    else
                    /// Find Exits Node
                    if(nodeIndex == nodeEnd)
                        m_endNode = new Vector2Int(x, y);

                    nodeIndex ++;
                }
            }
        }

        /// Once finished, throw an event.
        if(OnMapGenerationCompleteEvent != null)
            OnMapGenerationCompleteEvent(m_startNode, m_endNode, m_maxNodes);        
    }

    /// <summary>
    /// Check if a travel between two points is valid.
    /// </summary>
    /// <param name="start">Starting Point</param>
    /// <param name="end">Ending Point</param>
    /// <returns>true if they are connected, false if not.</returns>
    public bool IsValidTravel(Vector2Int start, Vector2Int end)
    {
        if(start == end)
            return false;
            
        int indexNode = -1;

        if(!m_map.FindFromNodes((x=>x.Value == start), out indexNode))
            return false;

        foreach(Node<Vector2Int> neighbor in m_map.Nodes[indexNode].Neighbors)
        {
            if(neighbor.Value == end)
                return true;
        }

        return false;
    }

    /// <summary>
    /// Check if a node exists and is recorded in the map.
    /// </summary>
    /// <param name="node">Node Position to check.</param>
    /// <returns>True if exists, false if not.</returns>
    public bool IsValidNode(Vector2Int node)
    {
        return m_map.NodeExists(node);
    }

    private int GetFixedNodeAtPosition(float position)
    {       
        int index = Mathf.Clamp(Mathf.RoundToInt(m_maxNodes * position), 0, m_maxNodes - 1);

        return index;
    }
}