﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProceduralGeneration;
using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.UI.Extensions;

public class MapDrawer : MonoBehaviour
{
    public GameObject       NodeGameObject;
    public GameObject       RouteGameObject;
    public RectTransform    NodeGroup;
    [SerializeField]
    private float           m_offset;

    private List<NodeController> NodesList = new List<NodeController>();

    private Map  m_map { get => MapController.Instance.Map; }

    void OnEnable()
    {
        NodesDataManager.Instance.OnNodesGeneratedEvent += GenerateMap;
    }

    void OnDisable()
    {
        NodesDataManager.Instance.OnNodesGeneratedEvent -= GenerateMap;
    }

    public void GenerateMap(List<NodeData> data)
    {
        if(MapController.Instance == null || MapController.Instance.Map == null)
        {
            Debug.LogError("[MapDrawer] No Instance of MapController found or No map was generated");
            return;
        }

        Vector3         _finalPosition;
        Vector3         _positionInRadius;
        GameObject      _gameObject;
        NodeController  _nodeController;

        /// Generate Nodes
        for (int i = 0; i < m_map.Nodes.Count; i++)
        {
            /// Create GameObject
            _gameObject         = Instantiate(NodeGameObject, NodeGroup);
            
            _positionInRadius   = UnityEngine.Random.insideUnitCircle;
            
            /// Generate a position in a radius
            _finalPosition                  = new Vector3((m_map.Nodes[i].Value.x + _positionInRadius.x- MapController.Instance.Map.GridSize.x / 2 ) * m_offset,
                                                        (m_map.Nodes[i].Value.y + _positionInRadius.y - MapController.Instance.Map.GridSize.y / 2 )* m_offset, 
                                                        0);
             
            _gameObject.transform.position  = _finalPosition;
            
            /// Add a Node Controller
            _nodeController                 = _gameObject.GetComponent<NodeController>();
            
            if(_nodeController == null)
                _nodeController = _gameObject.AddComponent<NodeController>();

            /// Find the correct data to add.
            int index = data.FindIndex((x)=> x.PositionGrid == m_map.Nodes[i].Value);
            
            if(index >= 0)
                _nodeController.Data = data[index];
    
            NodesList.Add(_nodeController);
        }

        /// Generate Routes
        GenerateRoutes();
    }

    public void GenerateRoutes()
    {
        if(RouteGameObject == null || NodesList == null || NodesList.Count == 0)
            return;

        m_map.DeepFirstSearch((origin, neighbour)=>PlaceRoute(origin, neighbour, RouteGameObject), m_map.Nodes[0]);
    }

    void PlaceRoute(Node<Vector2Int> origin, Node<Vector2Int> neighbour, GameObject prefab)
    {
       /* GameObject g = Instantiate(prefab, NodeGroup);
        UILineConnector line = g.GetComponent<UILineConnector>();
        
        if(line == null)
            return;

        line.transforms = new RectTransform[2];

        int indexNodeA = NodesList.FindIndex((x)=>x.Position==origin.Value.Position);
        if(indexNodeA < 0)
            return;

        line.transforms[0] = NodesList[indexNodeA].RectTransform;

        
        int indexNodeB = NodesList.FindIndex((x)=>x.Position==neighbour.Value.Position);
        if(indexNodeB < 0)
            return;

        line.transforms[1] = NodesList[indexNodeB].RectTransform;

        if(line.UILineRenderer != null)
        {
            line.UILineRenderer.Resoloution = Vector3.Distance(NodesList[indexNodeA].ScenePosition, NodesList[indexNodeB].ScenePosition);
        }

        g.name = "Route " + origin.Value.Position + " -> " + neighbour.Value.Position;
        g.transform.SetAsFirstSibling();*/
    }


}

