﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NodesDataModel
{
    [System.Serializable]
    public class PriorityNodeData
    {
        [Header("Node To Spawn")]
        public NodeData node;
        public bool isPositionFixed;

        [Header("Position To Spawn (1 last, 0 first)")]
        [Range(0.0f, 1.0f)]
        public float position;
    }

    [Header("Nodes Setup")]
    public List<PriorityNodeData> PriorityNodes;
    public List<NodeData> Nodes;

    /// <summary>
    /// Populate the SelectedNodes array in order to spawn just a
    /// selected list of nodes.
    /// </summary>
    /// <param name="maxRoomsToSpawn">Max Count of nodes to spawn.</param>
    /// <returns>True if operation is completed, false if cannot complete</returns>
    public bool GetNodesToSpawn(int maxRoomsToSpawn, out NodeData[] selectedNodes)
    {
        int counter = 0;
        selectedNodes = null;

        /// Check if Rooms to spawn is less than the max rooms
        /// requested in the dungeon.
        if (PriorityNodes != null && PriorityNodes.Count >= maxRoomsToSpawn)
        {
            Debug.LogError("[NodeDisposer]: Must Spawn Nodes Count is greater than the rooms count in the dungeon.");
            return false;
        }

        selectedNodes = new NodeData[maxRoomsToSpawn];

        /// Load the selected rooms with the must spawn rooms list.
        if (PriorityNodes != null && PriorityNodes.Count > 0)
        {
            for (counter = 0; counter < PriorityNodes.Count; counter++)
            {
                if (PriorityNodes[counter].isPositionFixed)
                {
                    int index = Mathf.RoundToInt(maxRoomsToSpawn * PriorityNodes[counter].position);

                    if (index >= maxRoomsToSpawn)
                        index = maxRoomsToSpawn - 1;

                    if (selectedNodes[index] != null)
                        Debug.LogWarning("[NodeDisposer]: Two Must Spawn Nodes with the same position id.");

                    selectedNodes[index] = ScriptableObject.Instantiate(PriorityNodes[counter].node);
                }
                else
                {
                    for (int i = 0; i < selectedNodes.Length; i++)
                    {
                        if (selectedNodes[i] == null)
                            selectedNodes[i] = ScriptableObject.Instantiate(PriorityNodes[counter].node);
                    }
                }
            }
        }

        if(Nodes != null && Nodes.Count > 0)
        {
            /// Than selected randomly rooms from the other list
            for (counter = 0; counter < maxRoomsToSpawn; counter++)
            {
                if (selectedNodes[counter] != null)
                    continue;

                int _rand = Random.Range(0, Nodes.Count);
                selectedNodes[counter] = ScriptableObject.Instantiate(Nodes[_rand]);
            }
        }

        return (selectedNodes != null && selectedNodes.Length >= maxRoomsToSpawn);
    }
}
