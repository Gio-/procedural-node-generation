﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProceduralGeneration
{
	public class MapModel 
	{
		private Vector2Int 		m_grid;
		private int		   		m_roomCount;
		private int				m_generatedRoom;
		private Map       		m_graph; 

		public Map GenerateMap(Vector2Int _grid, int _roomCount)
		{
			SetupGrid(_grid, _roomCount);

			m_graph = new Map(m_grid);

			/// Generate Rooms
			GenerateRooms();

			/// Add Doors 
			GenerateDoor();

			//Debug.Log(m_graph.Nodes.Count);

			return m_graph;
		}

		private void SetupGrid(Vector2Int _grid, int _roomCount)
		{
			m_grid      = new Vector2Int(_grid.x, _grid.y);
			m_roomCount = _roomCount;
			m_generatedRoom = 0;

			/// If Room Requested to be created is > than the available 
			/// space in grid, then we have to clamp the room requested 
			/// value.
			if (m_roomCount > (m_grid.x * m_grid.y))
			{
				m_roomCount = m_grid.x * m_grid.y;
			}
		}

		private void GenerateRooms()
		{
			/// 0-----------+
			/// |           |     0 = rooms[0,0]
			/// |     v     |     v = rooms[grid.x, grid.y] - this is the first room
			/// |           |     1 = rooms[grid.x * 2, grid.y * 2]
			/// |           |
			/// +-----------1
			/// Before we start generating rooms, we must place the first 
			/// one in the center of the grid to prevent a Top-Left dungeon
			/// generation map.

			/// Add first room
			Vector2Int firstNodePosition = new Vector2Int(Mathf.RoundToInt(m_grid.x / 2), Mathf.RoundToInt(m_grid.y / 2));
			m_graph.AddNode(new Node<Vector2Int>(firstNodePosition));
			m_generatedRoom ++;

			Vector2Int resultPosition = Vector2Int.zero;
			Vector2Int nodeNeighbour  = Vector2Int.zero;
			Node<Vector2Int> node;

			/// With a loop, we start generating rooms (-1 because we've already 
			/// created one).
			for(int i = 0; i < m_roomCount - 1; i++)
			{
				// Scrivo in italiano per i posteri. Il cuore di questo script è quello di
				// collegare delle room ed eseguire il "branching", tuttavia dobbiamo evitare
				// la possibilità di avere branch troppo lunghi. Per fare ciò, inseriamo un 
				// numero random che più andremo vicino alla fine del loop, più le possibilità 
				// saranno minori.
				float limitPercentage = i / m_roomCount;
				float branchingPercenetage = Mathf.Lerp(0.2f, 0.01f, limitPercentage);

				/// Get new position from random room created previously (+1 -1 on x or y of prec room)
				resultPosition = GetRandomPosition(out nodeNeighbour);

				/// if the new position founded has more than 1 neighbour but we have to "branch" 
				/// then, found a new position without neighbour.
				if(HasRoomNeighbour(resultPosition) > 1 && Random.value > branchingPercenetage)
				{
					int attempts = 0;
					do
					{
						resultPosition = GetRandomPosition(out nodeNeighbour, true);
						attempts++;
					} while (HasRoomNeighbour(resultPosition) > 1 && attempts < 150);
				}

				node = new Node<Vector2Int>(new Vector2Int(resultPosition.x, resultPosition.y));
				node.Neighbors.Add(new Node<Vector2Int>(nodeNeighbour));
				
				// add room starting from center.
				m_graph.AddNode(node, nodeNeighbour);

				m_generatedRoom ++;

				/// Reset Neighbour
				nodeNeighbour = new Vector2Int(-1, -1);
			}
		}

		private void GenerateDoor()
		{
			List<Direction> _listDirections = new List<Direction>();
			foreach(Node<Vector2Int> node in m_graph.Nodes)
			{
				GetRoomNeighbourDirections(node.Value, ref _listDirections);

				if(_listDirections.Count == 0)
					continue;
					
				foreach(Direction dir in _listDirections)
				{
					//node.Value.AddDoor(dir);
				}
			}
		}

		private Vector2Int GetRandomPosition(out Vector2Int link, bool hadToNotHaveNeighbour = false)
		{
			Vector2Int result = Vector2Int.zero;
			link 		      = Vector2Int.zero;

			int _index = 0,
			    _newX  = 0, 
			    _newY  = 0,
			    _count = 0;

			do
			{
				if (hadToNotHaveNeighbour || m_graph.Grid.Count == 0)
				{
					_index = GetRoomWithoutNeighbour();
				}
				else
				{
					_index = Random.Range(0, m_graph.Grid.Count - 1);
				}
				
				_newX = Mathf.RoundToInt(m_graph.Grid[_index].x);
				_newY = Mathf.RoundToInt(m_graph.Grid[_index].y);

				/// Searching in a new Direction:
				bool isUpAndDown = (Random.value > 0.5f);
				bool isPositive = (Random.value > 0.5f);

				if (isUpAndDown)
				{
					_newY = (isPositive) ? _newY + 1 : _newY - 1;
				}
				else
				{
					_newX = (isPositive) ? _newX + 1 : _newX - 1;
				}

				result = new Vector2Int(_newX, _newY);
				_count ++;
			} while (!m_graph.IsValidNodePosition(result) && _count < 50);

			if(_count >= 50){
				Debug.Log("[MapComposer]: No Valid Position Found");
				Debug.Break();
			}

			link = m_graph.Grid[_index];

			return result;
		}

		private int HasRoomNeighbour(Vector2Int _roomPosition)
		{
			short result = 0;
			
			if (m_graph.Grid.Contains(_roomPosition + Vector2Int.left))
				result++;

			if (m_graph.Grid.Contains(_roomPosition + Vector2Int.right))
				result++;

			if (m_graph.Grid.Contains(_roomPosition + Vector2Int.up))
				result++;

			if (m_graph.Grid.Contains(_roomPosition + Vector2Int.down))
				result++;

			return result;
		}

		private int GetRoomNeighbourDirections(Vector2Int _nodePosition, ref List<Direction> _directions)
		{
			_directions.Clear();

			if (m_graph.Grid.Contains(_nodePosition + Vector2Int.left))
				_directions.Add(Direction.West);

			if (m_graph.Grid.Contains(_nodePosition + Vector2Int.right))
				_directions.Add(Direction.East);

			if (m_graph.Grid.Contains(_nodePosition + Vector2Int.up))
				_directions.Add(Direction.North);

			if (m_graph.Grid.Contains(_nodePosition + Vector2Int.down))
				_directions.Add(Direction.South);
			
			return _directions.Count;
		}

		private int GetRoomWithoutNeighbour()
		{
			int index    = 0;
			int totLoops = 0;
			int maxLoops = 150;

			do
			{
				index = Random.Range(0, m_graph.Grid.Count - 1);
				totLoops++;
			} while (HasRoomNeighbour(m_graph.Grid[index]) > 1 && totLoops < maxLoops);

			if (totLoops >= maxLoops)
				Debug.LogWarning("[MapComposer]: Impossibile To find a room without neighbour!");

			return index;
		}
	}
}

