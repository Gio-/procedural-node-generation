﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProceduralGeneration
{
	[System.Serializable]
	public class Map : Graph<Vector2Int>
	{
		private List<Vector2Int> m_grid;
		private Vector2Int 		 m_gridSize;

		public  Vector2Int       GridSize   { get { return m_gridSize; } }
		public  List<Vector2Int> Grid 	    { get { return m_grid; 	   } }
		public  int				 CellCount  { get { return m_nodes.Count;}}

		/// <summary>
		/// Constructor 
		/// </summary>
		/// <param name="_grid">Set the grid size of the graph.</param>
		/// <returns></returns>
		public Map(Vector2Int _grid) : base()
		{
			m_gridSize = new Vector2Int(_grid.x, _grid.y);
			m_grid 	   = new List<Vector2Int>(); 
		}

		/// <summary>
		/// Add Node in the Graph
		/// </summary>
		/// <param name="_node">Node to add.</param>
		public override void AddNode(Node<Vector2Int> _node)
		{
			if(!IsValidNodePosition(_node.Value))
				return;

			if(!m_nodes.Contains(_node))
			{
				m_nodes.Add(_node);
			}

			if(!m_grid.Contains(_node.Value))
			{
				m_grid.Add(_node.Value);
			}
		}
		
		/// <summary>
		/// Add a node to the graph and specify its neighbor
		/// </summary>
		/// <param name="_node">Node to add.</param>
		/// <param name="_neighbor">Neighbor of that node.</param>
		public void AddNode(Node<Vector2Int> _node, Vector2Int _neighbor)
		{
			if(!IsValidNodePosition(_node.Value))
				return;

			if(m_nodes.Contains(_node))
				return;

			int index = m_grid.FindIndex((x)=>x == _neighbor);

			if(index > -1)
			{
				Node<Vector2Int> _nodeNeighbor = m_nodes[index];
				if(_nodeNeighbor != null)
				{
					AddNeighbors(_node, _nodeNeighbor);
				}
			}
				
			m_nodes.Add(_node);

			if(!m_grid.Contains(_node.Value))
			{
				m_grid.Add(_node.Value);
			}
		}

		/// <summary>
		/// Remove a node from the graph.
		/// </summary>
		/// <param name="_nodeToRemove">Node to remove.</param>
		public override void RemoveNode(Node<Vector2Int> _nodeToRemove)
		{
			base.RemoveNode(_nodeToRemove);

			m_grid.Remove(_nodeToRemove.Value);
		}
		
		/// <summary>
		/// Get a Node from its position in the grid.
		/// </summary>
		/// <param name="_position">Position to find.</param>
		/// <param name="_node"></param>
		/// <returns>return true if node exists, false if not</returns>
		public bool GetNodeFromPosition(Vector2Int _position, out Node<Vector2Int> _node)
		{
			_node = null;
			int _index = -1;

			if(!FindFromNodes((x)=> x.Value == _position, out _index))
				return false;

			_node = m_nodes[_index];
			return true;
		}

		/// <summary>
		/// Check if node exists at position
		/// </summary>
		/// <param name="_position">Position to check</param>
		/// <returns>True if exists, false if not.</returns>
		public bool NodeExists(Vector2Int _position)
		{
			int _index = -1;

			if(!FindFromNodes((x)=> x.Value == _position, out _index))
				return false;

			return (_index >= 0);
		}

		/// <summary>
		/// Check if the position is valid to place a node.
		/// </summary>
		/// <param name="_position">Position to check</param>
		/// <returns>True if is valid, false if not.</returns>
		public bool IsValidNodePosition(Vector2Int _position)
		{		
			if(m_grid.Contains(_position))
			{
				//Debug.LogWarning("[Map]: Node position already occupied!");
				return false;
			}

			if(_position.x > m_gridSize.x || _position.y > m_gridSize.y ||
			   _position.x < 0 || _position.y < 0)
			{

				//Debug.LogWarning("[Map]: Node position ("+ _position.x + "." + _position.y + ") + exceed the grid specification. (" + m_gridSize.x + "." + m_gridSize.y + ")");
				return false;
			}

			return true;
		}

		/// <summary>
		/// Get a node neighbor from its direction
		/// </summary>
		/// <param name="_currentNode">Node to check</param>
		/// <param name="_dir">direction to check</param>
		/// <param name="_neighbor">Neighbor founded</param>
		/// <returns>True if exists, false if not.</returns>
		public bool GetNeighborFromDirection(Node<Vector2Int> _currentNode, Direction _dir, out Vector2Int _neighbor)
		{
			int 	   _index 		  = -1;
			Vector2Int _posToSearch   = _currentNode.Value + Utilities.FromDirectionToVector(_dir);

			_neighbor = default(Vector2Int);

			for(int i = 0; i < _currentNode.Neighbors.Count; i ++)
			{
				if(_currentNode.Neighbors[i].Value == _posToSearch)
				{
					_index = i;
					break;
				}
			}

			if(!FindFromNodes((x)=>x.Value == _currentNode.Neighbors[_index].Value, out _index))
			{
				return false;
			}

			_neighbor = m_nodes[_index].Value;

			return true;
		}

		public bool FindFromNodes(System.Predicate<Node<Vector2Int>> predicate, out int index)
		{
			index = -1;

			if(m_nodes.Count == 0)
				return false;
			
			index = m_nodes.FindIndex(predicate);

			return (index < 0) ? false : true;
		}

		/// <summary>
		/// DFS method to iterate all graph from a start node.
		/// </summary>
		/// <param name="OnNodeVisited">Delegate to execute some code once node is visited.</param>
		/// <param name="startNode">Start Node.</param>
		public void DeepFirstSearch(System.Action<Node<Vector2Int>, Node<Vector2Int>> OnNodeVisited, Node<Vector2Int> startNode)
        {
			if(m_nodes == null || m_nodes.Count == 0)
				return;
				
            Node<Vector2Int> currentNode = null;
            Dictionary<Vector2Int, bool> visited = new Dictionary<Vector2Int, bool>();

            for (int i = 0; i < m_nodes.Count; i++)
            {
                visited.Add(m_nodes[i].Value, false);
            }

            //For DFS use stack
            Stack<Node<Vector2Int>> stack = new Stack<Node<Vector2Int>>();

            visited[startNode.Value] = true;

            stack.Push(startNode);

            while (stack.Count != 0)
            {
                currentNode = stack.Pop();

                foreach (Node<Vector2Int> neighbor in currentNode.Neighbors)
                {
                    if (!visited[neighbor.Value])
                    {
                        if (!visited.ContainsKey(neighbor.Value))
                            continue;

                        visited[neighbor.Value] = true;
                        stack.Push(neighbor);

                        /// Generate Route
                        OnNodeVisited(currentNode, neighbor);
                    }
                }
            }
        }
	}
}
