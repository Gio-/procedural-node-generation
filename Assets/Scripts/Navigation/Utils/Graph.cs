﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Graph<T>
{
	[SerializeField]
	protected List<Node<T>> m_nodes;

	public List<Node<T>> Nodes
	{
		get 
		{
			if(m_nodes == null)
			{
				m_nodes = new List<Node<T>>();
			}
			
			return m_nodes;
		}
	}

	public Graph()
	{
		m_nodes 	= new List<Node<T>>();
	}

	public virtual void AddNode(Node<T> _node)
	{
		if(!m_nodes.Contains(_node))
			Nodes.Add(_node);
	}

	public virtual void AddPair(Node<T> _firstNode, Node<T> _secondNode)
	{
		AddNode(_firstNode);
		AddNode(_secondNode);
		AddNeighbors(_firstNode, _secondNode);
	} 

	public virtual void AddNeighbors(Node<T> _firstNode, Node<T> _secondNode)
	{
		AddNeighbor(_firstNode, _secondNode);
		AddNeighbor(_secondNode, _firstNode);
	}

	public virtual void AddNeighbor(Node<T> _from, Node<T> _to)
	{
		if(!_from.Neighbors.Contains(_to))
			_from.Neighbors.Add(_to);
	}

	public virtual void RemovePairNeighbour(Node<T> _firstNode, Node<T> _secondNode)
	{
		if(_firstNode.Neighbors.Contains(_secondNode))
			_firstNode.Neighbors.Remove(_secondNode);

		if(_secondNode.Neighbors.Contains(_firstNode))
			_secondNode.Neighbors.Remove(_firstNode);		
	}

	public virtual void RemoveNode(Node<T> _nodeToRemove)
	{
		if(m_nodes.Contains(_nodeToRemove))
		{
			foreach(Node<T> node in m_nodes)
			{
				if(node.Neighbors.Contains(_nodeToRemove))
				{
					node.Neighbors.Remove(_nodeToRemove);
				}
			}

			m_nodes.Remove(_nodeToRemove);
		}
	}
}
