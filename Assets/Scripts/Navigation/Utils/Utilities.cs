﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utilities
{
    public static Direction InverseDirection(Direction direction)
    {
        Direction result;

        switch (direction)
        {
            case Direction.North:
                result = Direction.South;
                break;

            case Direction.South:
                result = Direction.North;
                break;

            case Direction.East:
                result = Direction.West;
                break;

            case Direction.West:
                result = Direction.East;
                break;

            case Direction.None:
            default:
                result = Direction.None;
                break;
        }

        return result;
    }

    public static Vector2Int FromDirectionToVector(Direction direction)
    {
        Vector2Int result;

        switch (direction)
        {
            case Direction.North:
                result = new Vector2Int(0, 1);
                break;

            case Direction.South:
                result = new Vector2Int(0, -1);
                break;

            case Direction.East:
                result = new Vector2Int(1, 0);
                break;

            case Direction.West:
                result = new Vector2Int(-1, 0);
                break;

            case Direction.None:
            default:
                result = new Vector2Int(0, 0);
                break;
        }

        return result;
    }
}
