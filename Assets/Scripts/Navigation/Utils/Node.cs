﻿using System.Collections.Generic;

[System.Serializable]
public class Node<T> 
{
	private T 			  m_value;
	private List<Node<T>> m_neighbors;

	/// <summary>
	/// Get the Node T value
	/// </summary>
	/// <value>return T value</value>
	public  T 			  Value 	{ get { return m_value; } }

	/// <summary>
	/// Get the neighbor list of this node.
	/// </summary>
	public  List<Node<T>> Neighbors
	{
		get
		{
			if(m_neighbors == null)
			{
				m_neighbors = new List<Node<T>>();
			}

			return m_neighbors;
		}
	}

	public Node(T _value)
	{
		m_value = _value;

		if(m_neighbors == null)
			m_neighbors = new List<Node<T>>();
	}
}
