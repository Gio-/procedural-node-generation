using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

[RequireComponent(typeof(Button))]
public class ButtonView : MonoBehaviour
{
    [SerializeField]
    private Button              m_button;                       /// Button.
    private bool                m_buttonPressed;                /// Flag cheking if button is pressed.

    private EventTrigger        m_eventTrigger;
    private EventTrigger.Entry  OnMousePress;
    private EventTrigger.Entry  OnMouseRelease;

    /// <summary>
    /// Get the UI Button.
    /// </summary>
    /// <value></value>
    public Button   Button        { get => m_button; }
    /// <summary>
    /// Flag checking if button is pressed. If value change, some
    /// event will be thrown.
    /// </summary>
    /// <value></value>
    public bool     IsButtonPressed 
    {
        get { return m_buttonPressed; }
        set 
        {
            if(value != m_buttonPressed)
            {
                m_buttonPressed = value;
                if(OnButtonChangeStatusEvent != null)
                    OnButtonChangeStatusEvent(value);
            }
        }
    }    

    public delegate void ButtonChangeStatusHandler(bool isPressed);
    public event ButtonChangeStatusHandler OnButtonChangeStatusEvent;
    
    #region Unity Methods
    void Start()
    {
        m_button = this.gameObject.GetComponent<Button>();

        /// Set up buttons event
        m_eventTrigger = m_button.GetComponent<EventTrigger>();
        if(m_eventTrigger == null)
            m_eventTrigger = m_button.gameObject.AddComponent<EventTrigger>();
        
        /// On Mouse Press
        OnMousePress = new EventTrigger.Entry();
        OnMousePress.eventID = EventTriggerType.PointerDown;
        OnMousePress.callback.AddListener((data) => { OnPointerDown((PointerEventData)data); });
        m_eventTrigger.triggers.Add(OnMousePress);

        /// On Mouse Release
        OnMouseRelease = new EventTrigger.Entry();
        OnMouseRelease.eventID = EventTriggerType.PointerUp;
        OnMouseRelease.callback.AddListener((data) => { OnPointerUp((PointerEventData)data); });
        m_eventTrigger.triggers.Add(OnMouseRelease);     
         
    }

    void OnDisable()
    {
        IsButtonPressed = false;
    }

    void OnDestroy()
    {
        m_eventTrigger.triggers.Clear();
        OnMousePress.callback.RemoveAllListeners();
        OnMouseRelease.callback.RemoveAllListeners();
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Override the status button and do not throw any
    /// event to notify the changes.
    /// </summary>
    /// <param name="isPressed">Set new status.</param>
    public void ForceButtonStatus(bool isPressed)
    {
        m_buttonPressed = isPressed;
    }
    #endregion

    #region Private Methods
    protected virtual void OnPointerUp(PointerEventData data)
    {
        IsButtonPressed = false;
    }

    protected virtual void OnPointerDown(PointerEventData data)
    {
        IsButtonPressed = true;
    }
    #endregion
}