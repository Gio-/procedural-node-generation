﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class NodeView : MonoBehaviour
{
    [SerializeField]
    private Button              m_button;

    [SerializeField]
    private Image               m_image;
    private Sprite              m_currentSprite;

    public Sprite               Sprite
    {
        get { return m_currentSprite; }
        set 
        {
            if(m_currentSprite != value)
            {
                m_currentSprite = value;

                if(m_image != null)
                    m_image.sprite = m_currentSprite;
            }

            if(OnChangeSpriteEvent != null)
                OnChangeSpriteEvent();
        }
    }

    public Button               Button
    {
        get { return m_button; }
    }

    public delegate void ChangeSpriteHandler();
    public event ChangeSpriteHandler OnChangeSpriteEvent;

    void Start()
    {
        if(m_button == null)
            m_button = GetComponent<Button>();

        if(m_image == null)
            m_image = GetComponentInChildren<Image>();
    }

}
