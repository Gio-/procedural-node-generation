using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class NodePanelView : ViewPresenter
{
    [SerializeField]
    private CanvasGroup m_canvasGroup;                              /// Canvas Group to enable or disable alpha in show or hide.
    [SerializeField]
    private bool        m_startDisabled         = false;            /// Start enabled or no.

    [Header("Components Group")]
    [SerializeField]
    private Text        m_labelName             = null;             /// Node Name
    [SerializeField]
    private Slider      m_buttonSlider          = null;             /// Slider loaded by pressing button.
    [SerializeField]
    private ButtonView  m_buttonView            = null;             /// The button view component.
    [SerializeField]
    private Vector2Int  m_currentDestination;                       /// Current recorded destination. Value that change every time a node controller
                                                                    /// open the NodePanel.

    /// <summary>
    /// UIText of the panel
    /// </summary>
    /// <value></value>
    public Text         LabelName       { get => m_labelName;    }
    /// <summary>
    /// Get the ButtonView of the Travel Button
    /// </summary>
    /// <value></value>
    public ButtonView   ButtonView      { get => m_buttonView;   }

    public delegate void OnRequestTravelHandler(Vector2Int destination);
    public event OnRequestTravelHandler RequestTravelEvent;

    #region Unity Methods
    void Start()
    {
        if(m_canvasGroup == null)
            m_canvasGroup = GetComponent<CanvasGroup>();  

        if(m_startDisabled)
            Hide();
    }

    void Update()
    {
        if(m_buttonView == null || m_buttonSlider == null)
            return;

        if(m_buttonView.IsButtonPressed)
        {
            m_buttonSlider.value = Mathf.Clamp01(m_buttonSlider.value + Time.deltaTime * 2.0f);

            if(m_buttonSlider.value >= 1.0f)
            {
                OnRequestTravel();
            }
        }
        else
        {
            if(m_buttonSlider.value > 0.0f)
                m_buttonSlider.value = Mathf.Clamp01(m_buttonSlider.value - Time.deltaTime * 2.0f);
        }
    }
    #endregion

    /// <summary>
    /// Show the Panel
    /// </summary>
    public override void Show()
    {
        m_canvasGroup.alpha = 1;
        
        base.Show();
    }
  
    /// <summary>
    /// Hide Panel
    /// </summary>
    public override void Hide()
    {
        m_canvasGroup.alpha  = 0;
        m_currentDestination = new Vector2Int(-1, -1); 
        
        base.Hide();
    }
    
    /// <summary>
    /// Change Current destination once player click
    /// on Start travel button.
    /// </summary>
    /// <param name="destination">New destination.</param>
    public void ChangeDestination(Vector2Int destination)
    {
        m_currentDestination = destination;
    }

    /// <summary>
    /// Show or Hide button in game.
    /// </summary>
    /// <param name="show"></param>
    public void ShowHideButton(bool show)
    {
        m_buttonView.gameObject.SetActive(show);
    }

    private void OnRequestTravel()
    {
        if (RequestTravelEvent != null)
            RequestTravelEvent(m_currentDestination);

        m_buttonSlider.value = 0.0f;
        m_buttonView.ForceButtonStatus(false);
        
        Hide();
    }
}