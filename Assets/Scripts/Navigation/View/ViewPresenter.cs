using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ViewPresenter : MonoBehaviour
{
    public RectTransform UIRoot { get; private set; }

    public delegate void ViewPresenterHandler();
    public event ViewPresenterHandler OnViewShowEvent;
    public event ViewPresenterHandler OnViewHideEvent;

    public void Awake()
    {
        UIRoot = this.transform.root.GetComponent<RectTransform>();
        
        OnAwake();
    }

    public virtual void OnAwake()
    {
        //
    }

    public virtual void Show()
    {
        //
        if(OnViewShowEvent != null) OnViewShowEvent();
    }

    public virtual void Hide()
    {
        //
        if(OnViewHideEvent != null) OnViewHideEvent();
    }



}