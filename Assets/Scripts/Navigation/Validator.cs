﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Validator<T, G> where G : IComparable
{
    private Dictionary<System.Func<T, bool>, G> rules;

    public Validator()
    {
        rules = new Dictionary<System.Func<T, bool>, G>();
    }

    /// <summary>
    /// Remove an already recorded rule
    /// </summary>
    /// <param name="rule"></param>
    public void RemoveRule(System.Func<T, bool> rule)
    {      
        if(rules.ContainsKey(rule))
            rules.Remove(rule);
    }

    /// <summary>
    /// Add a rule to the record
    /// </summary>
    /// <param name="rule">Rule to check</param>
    /// <param name="onError">Get some data on error.</param>
    public void AddRule(System.Func<T, bool> rule, G onError)
    {
        if(!rules.ContainsKey(rule))
            rules.Add(rule, onError);
    }

    /// <summary>
    /// Validate data based on recorded rules.
    /// </summary>
    /// <param name="arg">Argument to pass</param>
    /// <param name="errors">error to print. null if no error is printed.</param>
    /// <returns>true if is all valid, false if not.</returns>
    public bool IsValid(T arg, out G errors)
    {
        errors = default(G);

        if(rules == null || rules.Count == 0)
            return true;

        bool result = true;
        foreach(KeyValuePair<System.Func<T, bool>, G> rule in rules)
        {
            if(!rule.Key(arg))
            {
                errors = rule.Value;
                result = false;
                break;
            }
        }

        return result;
    }
}
